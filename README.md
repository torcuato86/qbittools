## Description

qbittools is a feature rich CLI for the management of torrents in qBittorrent.

## Table of contents

- [Description](#description)
- [Donate](#donate)
- [Table of contents](#table-of-contents)
- [Requirements](#requirements)
- [Installation](#installation)
  - [Binary version **(only Linux x86\_64)**](#binary-version-only-linux-x86_64)
  - [Docker image](#docker-image)
  - [Run as a python script](#run-as-a-python-script)
- [Building](#building)
  - [Building binary manually with Docker (optional)](#building-binary-manually-with-docker-optional)
- [Configuration](#configuration)
- [Usage](#usage)
  - [Help](#help)
  - [Self-upgrade](#self-upgrade)
  - [Subcommands](#subcommands)
    - [Add](#add)
      - [Operating system limits](#operating-system-limits)
      - [ruTorrent / AutoDL](#rutorrent--autodl)
    - [Unpause](#unpause)
      - [Automatic unpause in qBittorrent](#automatic-unpause-in-qbittorrent)
    - [Tagging](#tagging)
      - [Automatic tagging with Cron](#automatic-tagging-with-cron)
    - [Reannounce](#reannounce)
      - [Reannounce with systemd](#reannounce-with-systemd)
    - [Update passkey](#update-passkey)
    - [Export](#export)
    - [Mover](#mover)
      - [Automatic moving with Cron](#automatic-moving-with-cron)
    - [Orphaned](#orphaned)
  - [FlexGet](#flexget)

## Requirements

* Any usable Linux distribution (binary builds are built with musl and fully static starting from 0.4.0)
* ca-certificates (for connecting to https)

### Help
All commands have extensive help with all available options:

<details><summary>Click to expand</summary>

```bash
$ qbittools export -h
usage: qbittools export [-h] [-s 127.0.0.1] [-U username] [-P password] [-i ~/.local/share/qBittorrent/BT_backup] -o ~/export [-c mycategory] [-t [mytag ...]]

optional arguments:
  -h, --help            show this help message and exit
  -s 127.0.0.1, --server 127.0.0.1
                        host
  -U username, --username username
  -P password, --password password
  -i ~/.local/share/qBittorrent/BT_backup, --input ~/.local/share/qBittorrent/BT_backup
                        Path to qBittorrent .torrent files
  -o ~/export, --output ~/export
                        Path to where to save exported torrents
  -c mycategory, --category mycategory
                        Filter by category
  -t [mytag ...], --tags [mytag ...]
                        Filter by tags
```

</details>

### Subcommands
#### Tagging
Create useful tags to group torrents by tracker domains, not working trackers, unregistered torrents and duplicates
```bash
$ qbittools tagging --duplicates --unregistered --not-working --added-on --trackers
```

##### Automatic tagging with Cron
Execute every 10 minutes (`crontab -e` and add this entry)
```
*/10 * * * * /usr/local/bin/qbittools tagging --duplicates --unregistered --not-working --added-on --trackers
```

#### Reannounce
Automatic reannounce on problematic trackers (run in screen/tmux to prevent it from closing when you end a ssh session):

<details><summary>Click to expand</summary>


```bash
$ qbittools reannounce
07:40:40 PM --------------------------
07:40:40 PM [Movie.2020.2160p.WEB-DL.H264-GROUP] is not working, active for 1s, reannouncing...
07:41:20 PM --------------------------
07:41:20 PM [Movie.2020.2160p.WEB-DL.H264-GROUP] has no seeds, active for 78s, reannouncing...
07:41:25 PM --------------------------
07:41:25 PM [Movie.2020.2160p.WEB-DL.H264-GROUP] is active, progress: 0%
07:41:30 PM --------------------------
07:41:30 PM [Movie.2020.2160p.WEB-DL.H264-GROUP] is active, progress: 5.0%
07:41:35 PM --------------------------
07:41:35 PM [Movie.2020.2160p.WEB-DL.H264-GROUP] is active, progress: 11.1%
```

</details>

##### Reannounce with systemd
Reannounce can be executed and restarted on problems automatically by systemd. Create a new service at `/etc/systemd/system/` with the following contents:

<details><summary>Click to expand</summary>

```ini
[Unit]
Description=qbittools reannounce
After=qbittorrent@%i.service

[Service]
User=%i
Group=%i
ExecStart=/usr/local/bin/qbittools reannounce

Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

</details>

Restart the daemon with `systemctl daemon-reload` and start the service with `systemctl start qbittools-reannounce@username` by replacing username with the user you want to run it from. Check service logs with `journalctl -u qbittools-reannounce@username.service` if necessary.

#### Update passkey
Update passkey in all matching torrents (all tracker urls that match `--old` parameter):
```bash
$ qbittools update_passkey --old 12345 --new v3rrjmnfxwq3gfrgs9m37dvnfkvdbqnqc
2021-01-08 21:38:45,301 INFO:Replaced [https://trackerurl.net/12345/announce] to [https://trackerurl.net/v3rrjmnfxwq3gfrgs9m37dvnfkvdbqnqc/announce] in 10 torrents
```

#### Export
Export all matching .torrent files by category or tags:

<details><summary>Click to expand</summary>


```bash
$ qbittools export -o ./export --category movies --tags tracker.org mytag
01:23:43 PM INFO:Matched 47 torrents
01:23:43 PM INFO:Exported [movies] Fatman.2020.BluRay.1080p.TrueHD.5.1.AVC.REMUX-FraMeSToR [fbef10dc89bf8dff21a401d9304f62b074ffd6af].torrent
01:23:43 PM INFO:Exported [movies] La.Haine.1995.UHD.BluRay.2160p.DTS-HD.MA.5.1.DV.HEVC.REMUX-FraMeSToR [ee5ff82613c7fcd2672e2b60fc64375486f976ba].torrent
01:23:43 PM INFO:Exported [movies] Ip.Man.3.2015.UHD.BluRay.2160p.TrueHD.Atmos.7.1.DV.HEVC.REMUX-FraMeSToR [07da008f9c64fe4927ee18ac5c94292f61098a69].torrent
01:23:43 PM INFO:Exported [movies] Brazil.1985.Director's.Cut.BluRay.1080p.FLAC.2.0.AVC.REMUX-FraMeSToR [988e8749a9d3f07e5d216001efc938b732579c16].torrent
```

</details>

#### Mover
Useful for those who want to move torrents to different categories over time. Combined with enabled Automatic Torrent Management this will move files from one folder to another.

Move torrents inactive for more than 60 seconds and completed more than 60 minutes ago from categories `tracker1` and `tracker2` to category `lts` 
```bash
$ qbittools mover tracker1 tracker2 -d lts
```

Move torrents inactive for more than 600 seconds and completed more than 30 minutes ago from category `racing` to category `lts` 
```bash
$ qbittools mover racing -d lts --completion-threshold 30 --active-threshold 600
```

##### Automatic moving with Cron
Execute every 10 minutes (`crontab -e` and add this entry)
```
*/10 * * * * /usr/local/bin/qbittools mover racing -d lts
```

#### Orphaned

Find files no longer associated with any torrent, but still present in download folders (default download folder and folders from all categories). This command will remove orphaned files if you confirm it and also clean up all empty folders. _Be careful while removing a lot of files if you use these folders from other torrent client._

```bash
$ qbittools orphaned
```