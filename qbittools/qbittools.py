import argparse, logging, sys, os, yaml, utils
import qbittorrentapi
import importlib

logger = logging.getLogger(__name__)

def add_default_args(parser):
    parser.add_argument("-cf", "--config_trackers", metavar="/config/config.yaml", default="/config/config.yaml", required=False)
    parser.add_argument('-s', '--server', action=utils.EnvDefault, envvar="QBITTOOLS_SERVER", metavar='127.0.0.1:8080', help='qBittorrent host', required=False)
    parser.add_argument('-U', '--username', action=utils.EnvDefault, envvar="QBITTOOLS_USER", metavar='username', required=False)
    parser.add_argument('-P', '--password',action=utils.EnvDefault, envvar="QBITTOOLS_PASSWORD",  metavar='password', required=False)
    parser.add_argument('-k', '--disable-ssl-verify', help='disable SSL certificate verification when connecting to qBittorrent host', required=False, action='store_true')

def load_commands(subparsers):
    directory = "commands"

    def load_command(command):
        try:
            mod = importlib.import_module(f"{directory}.{command}")
            mod.add_arguments(command, subparsers)
            subparser = subparsers.choices.get(command)
            if subparser:
                add_default_args(subparser)
        except ImportError:
            logger.error(f"Error loading module: {command}", exc_info=True)
            sys.exit(1)
        else:
            globals()[command] = mod

    for cmd in os.listdir(f"{os.path.dirname(__file__)}/{directory}"):
        if cmd.startswith("__") or not cmd.endswith(".py"):
            continue
        load_command(cmd[:-3])

def qbit_client(app):
    client = qbittorrentapi.Client(host=app.server, username=app.username, password=app.password, VERIFY_WEBUI_CERTIFICATE=app.disable_ssl_verify)

    try:
        client.auth_log_in()
    except qbittorrentapi.APIConnectionError:
        logger.error("Error connecting to qBittorrent", exc_info=True)
        sys.exit(1)
    except qbittorrentapi.LoginFailed:
        logger.error("Login failed to qBittorrent", exc_info=True)
        sys.exit(1)
    else:
        return client

def get_config_trackers(app, key=None, default=None):
    try:
        with open(app.config_trackers, "r") as stream:
            config_trackers = yaml.safe_load(stream)
    except FileNotFoundError:
        logger.warning(f"Configuration file not found: {app.config}")
        config_trackers = {}
    except yaml.YAMLError as e:
        logger.error(f"Error parsing configuration file: {e}")
        sys.exit(1)

    return config_trackers.get(key, default) if key else config_trackers


def main():

    logging.getLogger("filelock").setLevel(logging.ERROR) # supress lock messages
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s %(levelname)s:%(message)s', datefmt='%I:%M:%S %p')

    parser = argparse.ArgumentParser(description="qBittorrent API Client")
    subparsers = parser.add_subparsers(dest='command')
    load_commands(subparsers) # Load all commands
    app = parser.parse_args()
    
    if not app.command:
        parser.print_help()
        sys.exit()

    app.client = qbit_client(app)
    app.config = get_config_trackers(app)

    try:
        mod = globals()[app.command]
        mod.__init__(app, logger)
    except Exception as e:
        logger.error(f"Error executing command '{app.command}': {e}")
        sys.exit(1)
    finally:
        app.client.auth_log_out()

if __name__ == "__main__":
    main()
