import collections

def __init__(app, logger):

    trackers = collections.defaultdict(int)

    for t in app.client.torrents.info():
        matches = list(map(lambda x: x.url, filter(lambda s: app.old in s.url, t.trackers)))
        found = len(matches) > 0

        if not found: continue

        for url in matches:
            trackers[url] += 1
            if not app.dry_run: t.edit_tracker(url, url.replace(app.old, app.new))

    for url in trackers:
        if app.dry_run:
            logger.info(f"Would replace [{url}] to [{url.replace(app.old, app.new)}] in {trackers[url]} torrents")
        else:
            logger.info(f"Replaced [{url}] to [{url.replace(app.old, app.new)}] in {trackers[url]} torrents")

    if len(trackers) == 0: logger.error(f"Not found any torrents matching {app.old} passkey")

def add_arguments(command, subparser):
    parser = subparser.add_parser(command)
    parser.add_argument('--old', metavar='oldpasskey', help='Old passkey', required=True)
    parser.add_argument('--new', metavar='newpasskey', help='New passkey', required=True)
    parser.add_argument('-d', '--dry-run', action='store_true')
