import os, operator
from pathlib import Path
from qbittools import utils

def __init__(app, logger):
    def confirm(text):
        answer = ""
        while answer not in ["y", "n"]:
            answer = input(text).lower()
        return answer == "y"

    def is_dir_empty(path):
        with os.scandir(path) as scan:
            return next(scan, None) is None

    def delete_empty_dirs(folders, force=False):
        logger.info("Collecting empty directories...")
        fs_dirs = [Path(path) / name for folder in folders for path, subdirs, files in os.walk(folder) for name in subdirs if is_dir_empty(Path(path) / name)]

        if len(fs_dirs) == 0:
            logger.info('None found')
            return

        for path in fs_dirs:
            print(path)

        if not force and not confirm('OK to delete all including parent directories if these are empty too [Y/N]? '):
            return

        for folder in folders:
            for path, subdirs, files in os.walk(folder, topdown=False):
                for name in subdirs:
                    dir_path = Path(path) / name

                    if is_dir_empty(dir_path):
                        logger.info(f"Removing {os.fsencode(dir_path).decode('utf8', 'replace')}")
                        dir_path.rmdir()

    logger.info(f"Checking for orphaned files on disk not in qBittorrent...")

    folders_dir = Path(app.client.application.preferences.save_path)
    folders = {folders_dir}
    folders |= {category.savePath for category in app.client.torrents_categories().values() if category.savePath != '' and not folders_dir.is_relative_to(category.savePath)}
    logger.info(f"Download folders: {list(map(str, folders))}")
    logger.info('Collecting files from download folders...')
    fs_files = {Path(path) / name for folder in folders for path, subdirs, files in os.walk(folder) for name in files}

    logger.info('Collecting files from qBittorrent...')
    qbit_files = {Path(torrent.save_path) / file.name for torrent in app.client.torrents.info() for file in torrent.files}
    diff = fs_files - qbit_files

    if len(diff) == 0:
        logger.info('Nothing to delete')
        delete_empty_dirs(folders, app.force)
        return


    logger.info('Preparing list of orphaned files...')
    files_and_sizes = ((path, path.stat().st_size) for path in diff)
    sorted_files_with_size = sorted(files_and_sizes, key = operator.itemgetter(1), reverse=True)

    total_size = sum(size for file, size in sorted_files_with_size)
    print(f"Total size: {utils.format_bytes(total_size)}")

    for file, size in sorted_files_with_size:
        print(f"{utils.format_bytes(size)}\t{os.path.relpath(os.fsencode(file).decode('utf8', 'replace'), folders_dir)}")

    if not app.force and not confirm('OK to delete all [Y/N]? '):
        return

    for file, size in sorted_files_with_size:
        logger.info(f"Removing {os.fsencode(file).decode('utf8', 'replace')}")
        file.unlink()
    delete_empty_dirs(folders, app.force)

def add_arguments(command, subparser):
    parser = subparser.add_parser(command)
    parser.add_argument('--force', action='store_true', help='Delete without asking')