import os, time, argparse
from pathlib import Path

def format_bytes(size):
    power = 2**10
    n = 0
    power_labels = {0 : 'B', 1: 'KiB', 2: 'MiB', 3: 'GiB', 4: 'TiB'}
    while size > power:
        size /= power
        n += 1
    formatted = round(size, 2)
    return f"{formatted} {power_labels[n]}"

def is_linked(path):
    path = Path(path)

    if os.path.islink(path):
        return True

    if os.path.isfile(path) and os.lstat(path).st_nlink > 1:
        return True

    if os.path.isdir(path):
        linked = [os.path.join(path, x) for path, subdirs, files in os.walk(path) for x in files if os.lstat(os.path.join(path, x)).st_nlink > 1 or os.path.islink(os.path.join(path, x))]
        return len(linked) > 0


def find_hard_links(path):
    if os.path.isfile(path) and os.lstat(path).st_nlink > 1:
        if find_hard_link_arr(path,'/mnt/TV Shows') or find_hard_link_arr(path,'/mnt/Movies'):
            return True
    if os.path.isdir(path):
        linked = [os.path.join(path, x) for path, subdirs, files in os.walk(path) for x in files if find_hard_link_arr(os.path.join(path, x),'/mnt/TV Shows') or find_hard_link_arr(os.path.join(path, x),'/mnt/Movies')]
        return len(linked) > 0

def find_hard_link_arr(path,arr_path):
    # Get the inode number of the target file
    target_inode = os.stat(path).st_ino
    target_device = os.stat(path).st_dev
    hard_links = []
    # Walk through the filesystem
    for root, dirs, files in os.walk(arr_path):
        for name in files:
            file = os.path.join(root, name)
            try:
                # Check if the inode number matches
                if os.stat(file).st_ino == target_inode and os.stat(file).st_dev == target_device:
                    hard_links.append(file)
            except FileNotFoundError:
                # Skip files that are not accessible
                continue
    return len(hard_links)==1

class EnvDefault(argparse.Action):
    def __init__(self, envvar, required=True, default=None, **kwargs):
        if envvar:
            if envvar in os.environ:
                default = os.environ[envvar]
        if required and default:
            required = False
        super(EnvDefault, self).__init__(default=default, required=required, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)