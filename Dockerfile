FROM python:3-alpine AS base

FROM base AS pip
WORKDIR /install
COPY requirements.txt /requirements.txt
RUN pip install --no-cache-dir --prefix=/install --requirement /requirements.txt \
    && python -c "import compileall; compileall.compile_path(maxlevels=10)"

FROM base AS app
WORKDIR /app
COPY qbittools/ .
RUN python -m compileall qbittools.py commands/

FROM base AS final
WORKDIR /app
COPY --from=pip /install /usr/local
COPY --from=app /app .
ENTRYPOINT ["python3", "qbittools.py"]